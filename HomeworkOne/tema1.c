//
//  sol1.c
//  Tema1
//
//  Created by Irimia Cosmin on 22/10/2019.
//  Copyright � 2019 Irimia Cosmin. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

int N = 100;
int R = 100;
int M = 100;

void initMatrixA(int matrixA[N][M]) {
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            matrixA[i][j] = i + j;
        }
    }
}

// void printMatrixA() {
//     for (int i = 0; i < N; i++) {
//         for (int j = 0; j < M; j++) {
//             printf("%d ", matrixA[i][j]);
//         }
//         printf("\n");
//     }
// }

void initMatrixB(int matrixB[M][R]) {
    for (int i = 0; i < M; i++) {
        for (int j = 0; j < R; j++) {
            matrixB[i][j] = i * j;
        }
    }
}

// void printMatrixB() {
//     for (int i = 0; i < M; i++) {
//         for (int j = 0; j < R; j++) {
//             printf("%d ", matrixB[i][j]);
//         }
//         printf("\n");
//     }
// }

void initMatrixC(int matrixC[N][R]) {
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < R; j++) {
            matrixC[i][j] = 0;
        }
    }
}

// void printMatrixC() {
//     for (int i = 0; i < N; i++) {
//         for (int j = 0; j < R; j++) {
//             printf("%d ", matrixC[i][j]);
//         }
//         printf("\n");
//     }
// }

// void printComparator() {
//     for (int i = 0; i < N; i++) {
//         for (int j = 0; j < R; j++) {
//             printf("%d ", comparator[i][j]);
//         }
//         printf("\n");
//     }
// }
//
//int areMatrixCEquals() {
//    for (int i = 0; i < N; i++) {
//        for (int j = 0; j < R; j++) {
//            if (matrixC[i][j] != comparator[i][j]) {
//                return -1;
//            }
//        }
//    }
//    return 1;
//}

double seqMultiplyMatrix(int matrixA[N][M], int matrixB[M][R], int matrixC[N][R]) {
    double start;
    double end;
    start = omp_get_wtime();

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < R; j++) {
            for (int k = 0; k < M; k++) {
                matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
            }

        }
    }

    end = omp_get_wtime();

    //printf("Sequential           - %f seconds | %d Threads \n", end - start, omp_get_thread_num());
    return end - start;
}

void compareMatrix() {
//    if (areMatrixCEquals(comparator, matrixC) > 0) {
//        printf("Matrix are equal\n\n");
//    } else {
//        printf("Matrix are not equal\n\n");
//    }
}
//
double parForOneMultiplyMatrix(int matrixA[N][M], int matrixB[M][R], int matrixC[N][R]) {
    double start;
    double end;
    int i, j, k;
    start = omp_get_wtime();
    #pragma omp parallel shared(matrixA, matrixB, matrixC) private(i, j, k)
    {
        for (i = 0; i < N; i++) {
            for (j = 0; j < R; j++) {
                #pragma omp for
                for (k = 0; k < M; k++) {
                    #pragma omp critical
                    matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
                }

            }
        }
    }
    end = omp_get_wtime();
    //printf("Parallel FOR 1       - %f seconds | %d Threads \n", end - start, omp_get_num_threads());
    //compareMatrix();
    return end - start;
}

double parForTwoMultiplyMatrix(int matrixA[N][M], int matrixB[M][R], int matrixC[N][R]) {
    double start;
    double end;
    start = omp_get_wtime();
    #pragma omp parallel shared(matrixA, matrixB, matrixC)
    {
        for (int i = 0; i < N; i++) {
            #pragma omp for
            for (int j = 0; j < R; j++) {

                for (int k = 0; k < M; k++) {
                    #pragma omp critical
                    matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
                }

            }
        }
    }
    end = omp_get_wtime();
    return end - start;
//    printf("Parallel FOR 2       - %f seconds | %d Threads \n", end - start, omp_get_num_threads());
//    compareMatrix();
}

double parForThreeMultiplyMatrix(int matrixA[N][M], int matrixB[M][R], int matrixC[N][R]) {
    double start;
    double end;
    start = omp_get_wtime();
    #pragma omp parallel shared(matrixA, matrixB, matrixC)
    {
        #pragma omp for
        for (int i = 0; i < N; i++) {

            for (int j = 0; j < R; j++) {

                for (int k = 0; k < M; k++) {
                    #pragma omp critical
                    matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
                }

            }
        }
    }
    end = omp_get_wtime();
    return end - start;
//    printf("Parallel FOR 3       - %f seconds | %d Threads \n", end - start, omp_get_num_threads());
//    compareMatrix();
}

double parCollapseForOneMultiplyMatrix(int matrixA[N][M], int matrixB[M][R], int matrixC[N][R]) {
    double start;
    double end;
    start = omp_get_wtime();
    int i, j, k;
    #pragma omp parallel for collapse(2) private(i, j, k)
    for (i = 0; i < N; i++) {
        for (j = 0; j < R; j++) {

            for (k = 0; k < M; k++) {
                #pragma omp critical
                matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
            }
        }
    }

    end = omp_get_wtime();
    return end - start;
//    printf("Collapse FOR 1       - %f seconds | %d Threads \n", end - start, omp_get_num_threads());
//    compareMatrix();
}

double parCollapseForTwoMultiplyMatrix(int matrixA[N][M], int matrixB[M][R], int matrixC[N][R]) {
    double start;
    double end;
    start = omp_get_wtime();
    int j, k;
    for (int i = 0; i < N; i++) {
        #pragma omp parallel for collapse(2) private(j, k)
        for (j = 0; j < R; j++) {
            for (k = 0; k < M; k++) {
                #pragma omp critical
                matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
            }
        }
    }

    end = omp_get_wtime();
    return end - start;
//    printf("Collapse FOR 2       - %f seconds | %d Threads \n", end - start, omp_get_num_threads());
//    compareMatrix();
}

double parCollapseForThreeMultiplyMatrix(int matrixA[N][M], int matrixB[M][R], int matrixC[N][R]) {
    double start;
    double end;
    start = omp_get_wtime();
    int i, j, k, threadNum;

    #pragma omp parallel for collapse(3) private(i, j, k)

    for (i = 0; i < N; i++) {
        for (j = 0; j < R; j++) {
            for (k = 0; k < M; k++) {
                #pragma omp critical
                matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
            }
        }
    }
    threadNum = omp_get_num_threads();
    end = omp_get_wtime();

//    printf("Collapse FOR 3       - %f seconds | %d Threads \n", end - start, threadNum);
//    compareMatrix();
    return end - start;
}

double parNestedForOneMultiplyMatrix(int matrixA[N][M], int matrixB[M][R], int matrixC[N][R]) {
    double start;
    double end;
    start = omp_get_wtime();
    int i, j, k;
    int threadNum = 0;
    #pragma omp parallel shared(matrixA, matrixB, matrixC, threadNum) private(i, j, k)
    {
        omp_set_nested(1);
        threadNum = omp_get_num_threads();
        #pragma omp for
        for (i = 0; i < N; i++) {
            #pragma omp parallel for
            for (j = 0; j < R; j++) {
                #pragma omp parallel for
                for (k = 0; k < M; k++) {
                    #pragma omp critical
                    matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
                }
            }
        }

        end = omp_get_wtime();
    }
//    printf("Nested FOR 1, 2, 3 - %f seconds | %d Threads \n", end - start, threadNum);
//    compareMatrix();
    return end - start;
}

double parNestedForTwoMultiplyMatrix(int matrixA[N][M], int matrixB[M][R], int matrixC[N][R]) {
    double start;
    double end;
    start = omp_get_wtime();
    int i, j, k, threadNum;
    #pragma omp parallel shared(matrixA, matrixB, matrixC, threadNum) private(i, j, k)
    {
        omp_set_nested(1);
        threadNum = omp_get_num_threads();

        #pragma omp for
        for (i = 0; i < N; i++) {
            #pragma omp parallel for
            for (j = 0; j < R; j++) {

                for (k = 0; k < M; k++) {
                    #pragma omp critical
                    matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
                }
            }
        }

        end = omp_get_wtime();
    }
    end = omp_get_wtime();
    return end - start;
//    printf("Nested FOR 1, 2    - %f seconds | %d Threads \n", end - start, threadNum);
//    compareMatrix();
}

double parNestedForThreeMultiplyMatrix(int matrixA[N][M], int matrixB[M][R], int matrixC[N][R]) {
    double start;
    double end;
    start = omp_get_wtime();
    int i, j, k, threadNum;
    #pragma omp parallel shared(matrixA, matrixB, matrixC, threadNum) private(i, j, k)
    {
        omp_set_nested(1);
        threadNum = omp_get_num_threads();

        #pragma omp for
        for (i = 0; i < N; i++) {
            for (j = 0; j < R; j++) {
                #pragma omp parallel for
                for (k = 0; k < M; k++) {
                    #pragma omp critical
                    matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
                }
            }
        }

    }
    end = omp_get_wtime();

//    printf("Nested FOR 1, 3    - %f seconds | %d Threads \n", end - start, threadNum);
//    compareMatrix();
    return end - start;
}

double parNestedForFourMultiplyMatrix(int matrixA[N][M], int matrixB[M][R], int matrixC[N][R]) {
    double start;
    double end;
    start = omp_get_wtime();
    int i, j, k, threadNum;
    #pragma omp parallel shared(matrixA, matrixB, matrixC, threadNum) private(i, j, k)
    {
        omp_set_nested(1);
        threadNum = omp_get_num_threads();
        for (i = 0; i < N; i++) {
            #pragma omp for
            for (j = 0; j < R; j++) {
                #pragma omp parallel for
                for (k = 0; k < M; k++) {
                    #pragma omp critical
                    matrixC[i][j] += matrixA[i][k] * matrixB[k][j];
                }
            }
        }
    }
    end = omp_get_wtime();

//    printf("Nested FOR 2, 3    - %f seconds | %d Threads \n", end - start, threadNum);
//    compareMatrix();
    return end - start;
}
//
//void printMatrices() {
//
//    printf("\n\n================================\n");
//    printf("N: %d, M: %d, R: %d\n", N, M, R);
//    initMatrixA();
//    initMatrixB();
//    initMatrixC();
//    seqMultiplyMatrix();
//
//    for (int i = 0; i < N; i++) {
//        for (int j = 0; j < R; j++) {
//            comparator[i][j] = matrixC[i][j];
//        }
//    }
//
//    initMatrixC();
//    parForOneMultiplyMatrix();
//
//    initMatrixC();
//    parForTwoMultiplyMatrix();
//
//    initMatrixC();
//    parForThreeMultiplyMatrix();
//
//    initMatrixC();
//    parCollapseForOneMultiplyMatrix();
//
//    initMatrixC();
//    parCollapseForTwoMultiplyMatrix();
//
//    initMatrixC();
//    parCollapseForThreeMultiplyMatrix();
//
//    initMatrixC();
//    parNestedForOneMultiplyMatrix();
//
//    initMatrixC();
//    parNestedForTwoMultiplyMatrix();
//
//    initMatrixC();
//    parNestedForThreeMultiplyMatrix();
//
//    initMatrixC();
//    parNestedForFourMultiplyMatrix();
//
//}

void redefine(int n, int m, int r) {
    N = n;
    M = m;
    R = r;
}

void computeMethod(int whatMethod, int verbose) {

    printf("METHOD : %d, verbose: %d \n", whatMethod, verbose);

    for(int k = 1; k < 5; k++) {
        for (int j = 1; j < 12; j+=5) {
            for (int i = 1; i<1001; i = i * 10) {
                i = i > 100 ? 800 : i;
                redefine(i, i, i);
                int matrixA[N][M], matrixB[M][R], matrixC[N][R];
                initMatrixA(matrixA);
                initMatrixB(matrixB);
                omp_set_num_threads(k);
                double sum = 0;
                for(int nrRuns = 0; nrRuns < j; nrRuns++) {
                    initMatrixC(matrixC);
                    switch (whatMethod) {
                        case 1: {
                            sum += seqMultiplyMatrix(matrixA, matrixB, matrixC);
                            break;
                        }
                        case 2: {
                            sum += parForOneMultiplyMatrix(matrixA, matrixB, matrixC);
                            break;
                        }
                       case 3: {
                           sum += parForTwoMultiplyMatrix(matrixA, matrixB, matrixC);
                           break;
                       }
                       case 4: {
                           sum += parForThreeMultiplyMatrix(matrixA, matrixB, matrixC);
                           break;
                       }
                       case 5: {
                           sum += parCollapseForOneMultiplyMatrix(matrixA, matrixB, matrixC);
                           break;
                       }
                       case 6: {
                           sum += parCollapseForTwoMultiplyMatrix(matrixA, matrixB, matrixC);
                           break;
                       }
                       case 7: {
                           sum += parCollapseForThreeMultiplyMatrix(matrixA, matrixB, matrixC);
                           break;
                       }
                       case 8: {
                           sum += parNestedForOneMultiplyMatrix(matrixA, matrixB, matrixC);
                           break;
                       }
                       case 9: {
                           sum += parNestedForTwoMultiplyMatrix(matrixA, matrixB, matrixC);
                           break;
                       }
                       case 10: {
                           sum += parNestedForThreeMultiplyMatrix(matrixA, matrixB, matrixC);
                           break;
                       }
                       case 11: {
                           sum += parNestedForFourMultiplyMatrix(matrixA, matrixB, matrixC);
                           break;
                       }
                    }
                }
                sum = sum / j;
                if (verbose == 0) {
                    printf("%f\n", sum);
                } else {
                    printf("N,M,R: %d, Time: %f, Th: %d, NrRuns: %d\n", i, sum, k, j == 1 ? 1 : j-1);
                }
            }
        }
    }
}

int main(int argc, const char *argv[]) {

    int option, verbose;
    printf("What method do you want to run tests for? ");
    printf("\n1.  Sequential");
    printf("\n2.  Parallel For 1");
    printf("\n3.  Parallel For 2");
    printf("\n4.  Parallel For 3");
    printf("\n5.  Collapse For 1");
    printf("\n6.  Collapse For 2");
    printf("\n7.  Collapse For 3");
    printf("\n8.  Nested For 123");
    printf("\n9.  Nested For 12");
    printf("\n10. Nested For 13");
    printf("\n11. Nested For 23\n... (1-11) ...");
    scanf("%d", &option);
    printf("\nVerbose? (1/0) ... ");
    scanf("%d", &verbose);

    if (option < 1 || option > 11 || verbose <0 || verbose>1) {
        return 0;
    }  
  
    computeMethod(option, verbose);
    return 0;
}
