#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include "mpi.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image/stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image/stb_image_write.h"

// Error utility macro
#define ON_ERROR_EXIT(cond, message) \
do { \
    if((cond)) { \
        printf("Error in function: %s at line %d\n", __func__, __LINE__); \
        perror((message)); \
        exit(1); \
    } \
} while(0)

#define min(X,Y) ((X) < (Y) ? (X) : (Y))
#define max(X,Y) ((X) > (Y) ? (X) : (Y))

// Check if a string "str" ends with a substring "ends"
static inline bool str_ends_in(const char *str, const char *ends) {
    char *pos = strrchr(str, '.');
    return !strcmp(pos, ends);
}

enum allocation_type {
    NO_ALLOCATION, SELF_ALLOCATED, STB_ALLOCATED
};

typedef struct {
    int width;
    int height;
    int channels;
    size_t size;
    uint8_t *data;
    enum allocation_type allocation_;
} Image;

double clamp(double x, double upper, double lower)
{
    return min(upper, max(x, lower));
}

void Image_load(Image *img, const char *fname) {
    if((img->data = stbi_load(fname, &img->width, &img->height, &img->channels, 0)) != NULL) {
        img->size = img->width * img->height * img->channels;
        img->allocation_ = STB_ALLOCATED;
    }
}

void Image_create(Image *img, int width, int height, int channels, bool zeroed) {
    size_t size = width * height * channels;
    if(zeroed) {
        img->data = calloc(size, 1);
    } else {
        img->data = malloc(size);
    }

    if(img->data != NULL) {
        img->width = width;
        img->height = height;
        img->size = size;
        img->channels = channels;
        img->allocation_ = SELF_ALLOCATED;
    }
}

void Image_save(const Image *img, const char *fname) {
    if(str_ends_in(fname, ".jpg") || str_ends_in(fname, ".JPG")
        || str_ends_in(fname, ".jpeg") || str_ends_in(fname, ".JPEG"))
    {
        stbi_write_jpg(fname, img->width, img->height, img->channels, img->data, 100);
    }
    else {
        ON_ERROR_EXIT(false, "");
    }
}

void Image_free(Image *img) {
    if(img->allocation_ != NO_ALLOCATION && img->data != NULL) {
        if(img->allocation_ == STB_ALLOCATED) {
            stbi_image_free(img->data);
        } else {
            free(img->data);
        }
        img->data = NULL;
        img->width = 0;
        img->height = 0;
        img->size = 0;
        img->allocation_ = NO_ALLOCATION;
    }
}

double ImageOperator(uint8_t *data,
                            const int width,
                            const int height,
                            const int channel,
                         uint8_t* gray_output){
    
    int index = 0, row, col;
    int flag = channel == 4 ? 2 : 1;
    int step = flag*width;
    int total = (channel * height) * (width * flag);
    int threadNum = omp_get_num_threads();
    int chunk_size = total / threadNum;
    double start;
    double end;
    start = omp_get_wtime();
    #pragma omp parallel for schedule (static, chunk_size)
    for (row = 0; row < channel * height; ++row) {
        for (col = 0; col < width * flag; col+=flag) {
            gray_output[index++] = (data[row*step+col]+
                                 data[row*step+col+1]+
                                 data[row*step+col+2])/3.0;
                
        }
    }

    end = omp_get_wtime();
    return end - start;
}

double opGray(int *input, int* output, int width, int height) {
    int index = 0, row, col;
    int threadNum = omp_get_num_threads();
    int size = width * height * 3;
    int chunk_size = size / threadNum;
    
    double start;
    double end;
    start = omp_get_wtime();
    #pragma omp parallel for schedule (static, chunk_size)
    for (row = 0; row < 3 * height; ++row) {
        for (col = 0; col < width; col++) {
            output[index++] = (input[row*width+col]+
                                 input[row*width+col+1]+
                                 input[row*width+col+2])/3.0;
                
        }
    }

    end = omp_get_wtime();
    return end - start;
}

double ImageOperatorSepia(uint8_t *data,
                            const int width,
                            const int height,
                            const int channel,
                         uint8_t* gray_output){
    
    int row;
    int flag = channel == 4 ? 2 : 1;
    int total = (channel * height) * (width * flag);
    int threadNum = omp_get_num_threads();
    int chunk_size = total / threadNum;
    
    double start;
    double end;
    start = omp_get_wtime();
    
    #pragma omp parallel for schedule (static, chunk_size)
    for (row = 0; row < height * width * channel; row+=3) { 
        gray_output[row] = (uint8_t) fmin(
                          (0.393 * data[row] 
                        + 0.769 * data[row + 1] 
                        + 0.189 * data[row + 2]), 255.0);
        gray_output[row + 1] = (uint8_t) fmin(
                          (0.349 * data[row] 
                        + 0.686 * data[row + 1] 
                        + 0.168 * data[row + 2]), 255.0);
        gray_output[row + 2] = (uint8_t) fmin(
                          (0.272 * data[row ] 
                        + 0.534 * data[row + 1] 
                        + 0.131 * data[row + 2]), 255.0);  

    }
    
    end = omp_get_wtime();
    return end - start;
}

double opSepia(int *input, int* output, int width, int height) {
    int threadNum = omp_get_num_threads();
    int size = width * height * 3;
    int chunk_size = size / threadNum;
    
    double start;
    double end;
    start = omp_get_wtime();
    #pragma omp parallel for schedule (static, chunk_size)
    for (int row = 0; row < size; row+=3) { 
            uint8_t one = input[row];
            uint8_t two = input[row + 1];
            uint8_t three = input[row + 2];
            output[row] = (uint8_t) fmin(
                              (0.393 * one 
                            + 0.769 * two 
                            + 0.189 * three), 255.0);
            output[row + 1] = (uint8_t) fmin(
                              (0.349 * one 
                            + 0.686 * two 
                            + 0.168 * three), 255.0);
            output[row + 2] = (uint8_t) fmin(
                              (0.272 * one
                            + 0.534 * two 
                            + 0.131 * three), 255.0);  
    }
    end = omp_get_wtime();
    return end - start;
}


double ImageOperatorNegative(uint8_t *data,
                            const int width,
                            const int height,
                            const int channel,
                         uint8_t* gray_output){

    int index = 0, row, col;
    int flag = channel == 4 ? 2 : 1;
    int step = flag*width;
    int total = (channel * height) * (width * flag);
    int threadNum = omp_get_num_threads();
    int chunk_size = total / threadNum;
    double start;
    double end;
    start = omp_get_wtime();
    
    #pragma omp parallel for schedule (static, chunk_size)
        for (row = 0; row < channel * height; ++row) {      
            for (col = 0; col < width * flag; col+=flag) {
                gray_output[index] = 255 - data[row*step+col];
                gray_output[index+1] = 255 - data[row*step+col+1];
                gray_output[index+2] = 255 - data[row*step+col+2];
                index++;
            }
        }
    
    end = omp_get_wtime();
    return end - start;
}

double opNegative(int *input, int* output, int width, int height) {
    int index = 0, row, col;
    int threadNum = omp_get_num_threads();
    int size = width * height * 3;
    int chunk_size = size / threadNum;
    
    double start;
    double end;
    start = omp_get_wtime();
    #pragma omp parallel for schedule (static, chunk_size)
    for (row = 0; row < 3 * height; ++row) {      
            for (col = 0; col < width; col++) {
                output[index] = 255 - input[row*width+col];
                output[index+1] = 255 - input[row*width+col+1];
                output[index+2] = 255 - input[row*width+col+2];
                index++;
            }
        }

    end = omp_get_wtime();
    return end - start;
}

double ImageOperatorBlur(uint8_t *data,
                            const int width,
                            const int height,
                            const int channel,
                         uint8_t* gray_output){
        
    int row, col, x, y;
    int total = (channel * height) * width;
    int threadNum = omp_get_num_threads();
    int chunk_size = total / threadNum;
    float filter[25] = {1/25.0, 1/25.0, 1/25.0, 1/25.0, 1/25.0, 
                       1/25.0, 1/25.0, 1/25.0, 1/25.0, 1/25.0,
                       1/25.0, 1/25.0, 1/25.0, 1/25.0, 1/25.0, 
                       1/25.0, 1/25.0, 1/25.0, 1/25.0, 1/25.0,
                       1/25.0, 1/25.0, 1/25.0, 1/25.0, 1/25.0};
    int stepVal = channel * width;
    int filterSize = 5;
    uint8_t sumRed, sumBlue, sumGreen;
    double start;
    double end;
    start = omp_get_wtime();
    #pragma omp parallel for schedule (static, chunk_size)        
    for(row = 0; row < height; row++) {
        for(col = 0; col < width; col++) {
            sumRed = 0;
            sumGreen = 0;
            sumBlue = 0;
            if(row > 2 && row < height - 2  && col > 2 && col < width - 2)
                for(x = -2; x <= 2; x++)
                    for(y = -2; y <= 2; y++)
                    {
                        sumRed += data[(row + x) * stepVal + (col + y) * channel] 
                                * filter[(x + 2) * filterSize + (y + 2)];
                        sumGreen += data[(row + x) * stepVal + (col + y) * channel + 1] 
                                * filter[(x + 2) * filterSize + (y + 2)];
                        sumBlue += data[(row + x) * stepVal + (col + y) * channel + 2] 
                                * filter[(x + 2) * filterSize + (y + 2)];
                    }  
            gray_output[stepVal * row + channel * col] = sumRed;
            gray_output[stepVal * row + channel * col + 1] = sumGreen;
            gray_output[stepVal * row + channel * col + 2] = sumBlue;
        }
    }
    end = omp_get_wtime();
    return end - start;
}

double opBlur(int *input, int* output, int width, int height) {
    int row, col, x, y;
    int threadNum = omp_get_num_threads();
    int size = width * height * 3;
    int chunk_size = size / threadNum;
    
    double start;
    double end;
    int sumRed, sumBlue, sumGreen;
    int channel = 3;
    int stepVal = channel * width;
    int filterSize = 5;
    float filter[25] = {1/25.0, 1/25.0, 1/25.0, 1/25.0, 1/25.0, 
                       1/25.0, 1/25.0, 1/25.0, 1/25.0, 1/25.0,
                       1/25.0, 1/25.0, 1/25.0, 1/25.0, 1/25.0, 
                       1/25.0, 1/25.0, 1/25.0, 1/25.0, 1/25.0,
                       1/25.0, 1/25.0, 1/25.0, 1/25.0, 1/25.0};

    // temp
    for(int y = 0; y < size; y++) {
        output[y] = input[y];
    }
    // temp end
    start = omp_get_wtime();
    #pragma omp parallel for schedule (static, chunk_size)
    for(row = 0; row < height; row++) {
        for(col = 0; col < width; col++) {
            sumRed = 0;
            sumGreen = 0;
            sumBlue = 0;
            if(row > 2 && row < height - 2  && col > 2 && col < width - 2) {
                for(x = -2; x <= 2; x++)
                    for(y = -2; y <= 2; y++)
                    {
                        sumRed += input[(row + x) * stepVal + (col + y) * channel] 
                                * filter[(x + 2) * filterSize + (y + 2)];
                        sumGreen += input[(row + x) * stepVal + (col + y) * channel + 1] 
                                * filter[(x + 2) * filterSize + (y + 2)];
                        sumBlue += input[(row + x) * stepVal + (col + y) * channel + 2] 
                                * filter[(x + 2) * filterSize + (y + 2)];
                    }
                output[stepVal * row + channel * col] = sumRed;
                output[stepVal * row + channel * col + 1] = sumGreen;
                output[stepVal * row + channel * col + 2] = sumBlue;  
            }   
        }
    }
    end = omp_get_wtime();
    return end - start;
}


double ImageOperatorRotate(uint8_t *data,
                            const int width,
                            const int height,
                            const int channel,
                         uint8_t* gray_output){

    int row, col;
    int stepVal = channel * width;
    int total = (channel * height) * width;
    int threadNum = omp_get_num_threads();
    int chunk_size = total / threadNum;
    int leftValue, rightValue;

    double start;
    double end;
    start = omp_get_wtime();

    #pragma omp parallel for schedule (static, chunk_size)
    for (row = 0; row < height; row++) {
        for (col = 0; col < width; col++) {
            leftValue = stepVal * row + channel * col;
            rightValue = stepVal * (row + 1) - channel * (col + 1);

            gray_output[leftValue] = data[rightValue];
            gray_output[leftValue + 1] = data[rightValue + 1];
            gray_output[leftValue + 2] = data[rightValue + 2];

            gray_output[rightValue] = data[leftValue];
            gray_output[rightValue + 1] = data[leftValue + 1];
            gray_output[rightValue + 2] = data[leftValue + 2];
        }
    }
    end = omp_get_wtime();
    return end - start;
}

double opFlip(int *input, int* output, int width, int height) {
    int row, col;
    int threadNum = omp_get_num_threads();
    int size = width * height * 3;
    int chunk_size = size / threadNum;
    int leftValue, rightValue;
    int channel = 3;
    int stepVal = channel * width;

    double start;
    double end;
    start = omp_get_wtime();
    #pragma omp parallel for schedule (static, chunk_size)
    for (row = 0; row < height; row++) {
        for (col = 0; col < width; col++) {
            leftValue = stepVal * row + 3 * col;
            rightValue = stepVal * (row + 1) - 3 * (col + 1);

            output[leftValue] = input[rightValue];
            output[leftValue + 1] = input[rightValue + 1];
            output[leftValue + 2] = input[rightValue + 2];

            output[rightValue] = input[leftValue];
            output[rightValue + 1] = input[leftValue + 1];
            output[rightValue + 2] = input[leftValue + 2];
        }
    }
    end = omp_get_wtime();
    return end - start;
}

char * getNewImageName(char imageName[50], char imageType[20]) {
    char * new_str ;
    if((new_str = malloc(strlen(imageName)+strlen(imageType)+1)) != NULL){
        new_str[0] = '\0';
        strcat(new_str,imageName);
        strcat(new_str,imageType);
    }else{
        printf("Error on malloc\n");
    }
    return new_str;
}


void printOptions(Image image, char imageName[50]) {

    int optionSelected = 0;
    Image imageProcessed;


    while(1) {
        printf("\n===========================");
        printf("\nWhat operations whould you like to do with the photo?");
        printf("\n===========================");
        printf("\n1. Grayscale");
        printf("\n2. Sepia");
        printf("\n3. Negative");
        printf("\n4. Mirror");
        printf("\n5. Blur");
        printf("\n-1. Exit");
        printf("\nPlease select an option:");
        scanf("%d", &optionSelected);
        switch (optionSelected) {
            case -1: {
                printf("\nOk, see ya!\n");
                exit(0);
            }
            case 1: {
                printf("w:%d, h:%d\n", image.width, image.height);
                Image_create(&imageProcessed, image.width, image.height, image.channels, false);
                ImageOperator(image.data, image.width,
                    image.height, image.channels, imageProcessed.data);
                Image_save(&imageProcessed, getNewImageName(imageName, "-gray.jpg"));
                Image_free(&imageProcessed);
                break;
            }
            case 2: {
                printf("w:%d, h:%d\n", image.width, image.height);
                Image_create(&imageProcessed, image.width, image.height, image.channels, false);
                ImageOperatorSepia(image.data, image.width,
                    image.height, image.channels, imageProcessed.data);
                Image_save(&imageProcessed, getNewImageName(imageName, "-sepia.jpg"));
                Image_free(&imageProcessed);
                break;
            }
            case 3: {
                printf("w:%d, h:%d\n", image.width, image.height);
                Image_create(&imageProcessed, image.width, image.height, image.channels, false);
                ImageOperatorNegative(image.data, image.width,
                    image.height, image.channels, imageProcessed.data);
                Image_save(&imageProcessed, getNewImageName(imageName, "-negative.jpg"));
                Image_free(&imageProcessed);
                break;
            }
            case 4: {
                printf("w:%d, h:%d\n", image.width, image.height);
                Image_create(&imageProcessed, image.width, image.height, image.channels, false);
                ImageOperatorRotate(image.data, image.width,
                    image.height, image.channels, imageProcessed.data);
                Image_save(&imageProcessed, getNewImageName(imageName, "-rotated.jpg"));
                Image_free(&imageProcessed);
                break;
            }
            case 5: {
                printf("w:%d, h:%d\n", image.width, image.height);
                Image_create(&imageProcessed, image.width, image.height, image.channels, false);
                ImageOperatorBlur(image.data, image.width,
                    image.height, image.channels, imageProcessed.data);
                Image_save(&imageProcessed, getNewImageName(imageName, "-blured.jpg"));
                Image_free(&imageProcessed);
                break;
            }
            default: {
                printf("\nThis option is not available!\n");
                break;
            }
        }
        printf("\nWould you want to process the photo more? (1/0)");
        scanf("%d", &optionSelected);
        if (optionSelected != 1) {
            printf("\nOk, see ya!\n");
            exit(0);
        }
    }

}

void automatedTesting(bool verbose) {
    printf("Starting automated tests\n");

    int threadNum = 4;
    int numberOfRuns = 1;
    double sumOfTiming = 0.0;

    const char *pictures[8];
    pictures[0] = "fruits.jpg";
    pictures[1] = "nature.jpg";
    pictures[2] = "colors.jpg";
    pictures[3] = "beach.jpg";
    pictures[4] = "400x400.jpg";
    pictures[5] = "800x400.jpg";
    pictures[6] = "1600x900.jpg";
    pictures[7] = "4000x2200.jpg";

    Image image, imageProcessed;

    for(int picIndex = 0; picIndex < 8; picIndex++) {
        Image_load(&image, pictures[picIndex]);

        for(int th = 1; th <= threadNum; th++) {
            omp_set_num_threads(th);
            
            for(int runNum = 1; runNum < 10; runNum*=10) {
                numberOfRuns = runNum;

                sumOfTiming = 0;
                for(int run = 1; run <= numberOfRuns; run++) {
                    Image_create(&imageProcessed, image.width, image.height, image.channels, false);
                    sumOfTiming += ImageOperator(image.data, image.width, image.height, image.channels, imageProcessed.data);
                    Image_free(&imageProcessed);
                }
                if (verbose) {
                    printf("Pic: %s | Threads: %d | Time: %f | Op: Grayscale | NumberOfRuns: %d \n", pictures[picIndex], th, 
                    sumOfTiming / runNum, runNum);
                } else {
                    printf("%f\n", sumOfTiming / runNum);
                }
                
                sumOfTiming = 0;
                for(int run = 1; run <= numberOfRuns; run++) {
                    Image_create(&imageProcessed, image.width, image.height, image.channels, false);
                    sumOfTiming += ImageOperatorSepia(image.data, image.width, image.height, image.channels, imageProcessed.data);
                    Image_free(&imageProcessed);
                }
               
                if (verbose) {
                    printf("Pic: %s | Threads: %d | Time: %f | Op: Sepia     | NumberOfRuns: %d \n", pictures[picIndex], th, 
                    sumOfTiming / runNum, runNum);
                } else {
                    printf("%f\n", sumOfTiming / runNum);
                }

                sumOfTiming = 0;
                for(int run = 1; run <= numberOfRuns; run++) {
                    Image_create(&imageProcessed, image.width, image.height, image.channels, false);
                    sumOfTiming += ImageOperatorNegative(image.data, image.width, image.height, image.channels, imageProcessed.data);
                    Image_free(&imageProcessed);
                }
                
                if (verbose) {
                    printf("Pic: %s | Threads: %d | Time: %f | Op: Negative  | NumberOfRuns: %d \n", pictures[picIndex], th, 
                    sumOfTiming / runNum, runNum);
                } else {
                    printf("%f\n", sumOfTiming / runNum);
                }

                sumOfTiming = 0;
                for(int run = 1; run <= numberOfRuns; run++) {
                    Image_create(&imageProcessed, image.width, image.height, image.channels, false);
                    sumOfTiming += ImageOperatorRotate(image.data, image.width, image.height, image.channels, imageProcessed.data);
                    Image_free(&imageProcessed);
                }
                
                if (verbose) {
                    printf("Pic: %s | Threads: %d | Time: %f | Op: Rotate    | NumberOfRuns: %d \n", pictures[picIndex], th, 
                    sumOfTiming / runNum, runNum);
                } else {
                    printf("%f\n", sumOfTiming / runNum);
                }

                sumOfTiming = 0;
                for(int run = 1; run <= numberOfRuns; run++) {
                    Image_create(&imageProcessed, image.width, image.height, image.channels, false);
                    sumOfTiming += ImageOperatorBlur(image.data, image.width, image.height, image.channels, imageProcessed.data);
                    Image_free(&imageProcessed);
                }
                
                if (verbose) {
                    printf("Pic: %s | Threads: %d | Time: %f | Op: Blur      | NumberOfRuns: %d \n", pictures[picIndex], th, 
                    sumOfTiming / runNum, runNum);
                } else {
                    printf("%f\n", sumOfTiming / runNum);
                }
            }
        }
    }
}

void manualTesting() {  
    char imageName[50];
    Image image;

    printf("\n===========================");
    printf("\n=== Welcome to ACetrica ===");
    printf("\n===========================");
    printf("\nPlease write the name of the photo you want to edit:");

    bool isImageLoaded = false;
    while (!isImageLoaded) {
        scanf("%s", imageName);
        Image_load(&image, imageName);
        if (image.data == NULL) {
            printf("\nError in loading the image. Please try again...\nImage name: ");
        } else {
            isImageLoaded = true;
        }
    }
    printOptions(image, imageName);
}

// int main(void) {

//     manualTesting();
//     //automatedTesting(true);

//     //  gcc -std=c11 -fopenmp -Wall -pedantic tema2.c -o tema2 -lm
//     // mpirun -np 4 ./hello
//     // mpirun -np 5 --hostfile mpi_hosts ./hello
//     // mpirun -np 5 --hostfile mpi_hosts ./hello
//     // mpicc -o hello helloworld2.c 
// }

double mpiRun( int argc, char *argv[], char imageName[50], char operation[50]) {
    Image image;
    Image imageProcessed;
    double start;
    double end;
    start = omp_get_wtime();
    int pid, np, n; 
    MPI_Status status; 
    MPI_Comm_rank(MPI_COMM_WORLD, &pid); 
    MPI_Comm_size(MPI_COMM_WORLD, &np); 

    if (pid == 0) { 

        Image_load(&image, imageName);
        Image_create(&imageProcessed, image.width, image.height, image.channels, false);

        printf("Working with image %s with size %d, doing operation %s\n", imageName, image.size, operation);


        n = image.size;
        int i, elements_per_slave = n / (np-1); 
        int rowCnt = image.width * 3;

        while(elements_per_slave % rowCnt != 0) {
            elements_per_slave ++;
        }

        int cnt=0;
        if (np > 1) { 
            int *subBuffer;
            subBuffer = malloc(elements_per_slave * sizeof(int));
            int startIndex, k;
            for (i = 1; i < np; i++) {

                startIndex = (i-1) * elements_per_slave; 
                k=0;
                for (int j = startIndex; j < startIndex + elements_per_slave; j++) {
                    subBuffer[k++] = image.data[j];
                }

                MPI_Send(&elements_per_slave, 
                        1, MPI_INT, i, 0, 
                        MPI_COMM_WORLD); 
                MPI_Send(&image.width, 
                        1, MPI_INT, i, 0, 
                        MPI_COMM_WORLD); 
                MPI_Send(&image.height, 
                        1, MPI_INT, i, 0, 
                        MPI_COMM_WORLD);
                MPI_Send(subBuffer, 
                        elements_per_slave, 
                        MPI_INT, i, 0, 
                        MPI_COMM_WORLD); 
            }

            int childPid;
            for (i = 1; i < np; i++) {

                startIndex = (i-1) * elements_per_slave; 
                k=0;
                
                MPI_Recv(subBuffer, elements_per_slave, MPI_INT, 
                    MPI_ANY_SOURCE, 0, 
                    MPI_COMM_WORLD, 
                    &status); 

                childPid = status.MPI_SOURCE;

                int aux = (childPid-1) * elements_per_slave;
                cnt = 0;
                for(int j = 0; j < elements_per_slave; j++) {
                    imageProcessed.data[aux + cnt] = (uint8_t) subBuffer[j];
                    cnt++;
                }
            }         
          
        } 
        Image_save(&imageProcessed, getNewImageName(imageName, operation));
        Image_free(&imageProcessed);   
        end = omp_get_wtime();
        return end - start;
    } 
    else { 
        int n_elements_recieved, width, height;
        int *slaveTemp;
        int *slaveTemp2;

        MPI_Recv(&n_elements_recieved, 
                1, MPI_INT, 0, 0, 
                MPI_COMM_WORLD, 
                &status); 

        MPI_Recv(&width, 
                1, MPI_INT, 0, 0, 
                MPI_COMM_WORLD, 
                &status); 

        MPI_Recv(&height, 
                1, MPI_INT, 0, 0, 
                MPI_COMM_WORLD, 
                &status); 


        slaveTemp = malloc(n_elements_recieved * sizeof(int) + 1);
        slaveTemp2 = malloc(n_elements_recieved * sizeof(int) + 1);

        MPI_Recv(slaveTemp, n_elements_recieved, 
                MPI_INT, 0, 0, 
                MPI_COMM_WORLD, 
                &status); 

        int h = n_elements_recieved / (width * 3);

        if (operation == "-gray.jpg") {
            opGray(slaveTemp, slaveTemp2, width, h);
        } else if (operation == "-sepia.jpg") {
            opSepia(slaveTemp, slaveTemp2, width, h);
        } else if (operation == "-negative.jpg") {
            opNegative(slaveTemp, slaveTemp2, width, h);
        } else if (operation == "-flip.jpg") {
            opFlip(slaveTemp, slaveTemp2, width, h);
        } else if (operation == "-blur.jpg") {
            opBlur(slaveTemp, slaveTemp2, width, h);
        }

        MPI_Send(slaveTemp2, n_elements_recieved, MPI_INT, 
                0, 0, MPI_COMM_WORLD);
        return 0.0;
    } 

    

    
}

void mpiAutomatic(int argc, char *argv[], bool verbose) {
    int pid; 

    printf("Starting automated tests\n");

    int threadNum = 4;
    int numberOfRuns = 1;
    double sumOfTiming = 0.0;


    char *pictures[8];
    pictures[0] = "fruits.jpg";
    pictures[1] = "nature.jpg";
    pictures[2] = "colors.jpg";
    pictures[3] = "beach.jpg";
    pictures[4] = "400x400.jpg";
    pictures[5] = "800x400.jpg";
    pictures[6] = "1600x900.jpg";
    pictures[7] = "4000x2200.jpg";
    for(int picIndex = 0; picIndex < 8; picIndex++) {


        for(int th = 1; th <= 1; th++) {
            omp_set_num_threads(th);
            
            for(int runNum = 1; runNum < 10; runNum*=10) {
                numberOfRuns = runNum;

                sumOfTiming = 0;
                for(int run = 1; run <= numberOfRuns; run++) {
                    sumOfTiming += mpiRun(argc, argv, pictures[picIndex], "-gray.jpg");
                }
                if(sumOfTiming != 0) {
                    if (verbose) {
                    printf("Pic: %s | Threads: %d | Time: %f | Op: Grayscale | NumberOfRuns: %d \n", pictures[picIndex], th, 
                    sumOfTiming / runNum, runNum);
                    } else {
                        printf("%f\n", sumOfTiming / runNum);
                    }
                }
                
                
                sumOfTiming = 0;
                for(int run = 1; run <= numberOfRuns; run++) {
                    sumOfTiming += mpiRun(argc, argv, pictures[picIndex], "-sepia.jpg");
                }
                
                if(sumOfTiming != 0) {
                    if (verbose) {
                        printf("Pic: %s | Threads: %d | Time: %f | Op: Sepia     | NumberOfRuns: %d \n", pictures[picIndex], th, 
                        sumOfTiming / runNum, runNum);
                    } else {
                        printf("%f\n", sumOfTiming / runNum);
                    }
                }

                sumOfTiming = 0;
                for(int run = 1; run <= numberOfRuns; run++) {
                    sumOfTiming += mpiRun(argc, argv, pictures[picIndex], "-negative.jpg");
                }
                
                if(sumOfTiming != 0) {
                    if (verbose) {
                        printf("Pic: %s | Threads: %d | Time: %f | Op: Negative  | NumberOfRuns: %d \n", pictures[picIndex], th, 
                        sumOfTiming / runNum, runNum);
                    } else {
                        printf("%f\n", sumOfTiming / runNum);
                    }
                }

                sumOfTiming = 0;
                for(int run = 1; run <= numberOfRuns; run++) {
                    sumOfTiming += mpiRun(argc, argv, pictures[picIndex], "-flip.jpg");
                }
                
                if(sumOfTiming != 0) {
                    if (verbose) {
                        printf("Pic: %s | Threads: %d | Time: %f | Op: Rotate    | NumberOfRuns: %d \n", pictures[picIndex], th, 
                        sumOfTiming / runNum, runNum);
                    } else {
                        printf("%f\n", sumOfTiming / runNum);
                    }
                }

                sumOfTiming = 0;
                for(int run = 1; run <= numberOfRuns; run++) {
                     sumOfTiming += mpiRun(argc, argv, pictures[picIndex], "-blur.jpg");
                }
                
                if(sumOfTiming != 0) {
                    if (verbose) {
                        printf("Pic: %s | Threads: %d | Time: %f | Op: Blur      | NumberOfRuns: %d \n", pictures[picIndex], th, 
                        sumOfTiming / runNum, runNum);
                    } else {
                        printf("%f\n", sumOfTiming / runNum);
                    }
                }
            }
        }

    }
}


int main( int argc, char *argv[] )
{  
     MPI_Init(&argc, &argv); 
     char *pictures[8];
     pictures[0] = "fruits.jpg";
     pictures[1] = "nature.jpg";
     pictures[2] = "colors.jpg";
     pictures[3] = "beach.jpg";
     pictures[4] = "400x400.jpg";
     pictures[5] = "800x400.jpg";
     pictures[6] = "1600x900.jpg";
     pictures[7] = "4000x2200.jpg";
     int picIndex = 4;
     mpiRun(argc, argv, pictures[picIndex], "-gray.jpg");
     mpiRun(argc, argv, pictures[picIndex], "-sepia.jpg");
     mpiRun(argc, argv, pictures[picIndex], "-negative.jpg");
     mpiRun(argc, argv, pictures[picIndex], "-flip.jpg");
     mpiRun(argc, argv, pictures[picIndex], "-blur.jpg");

    //mpiAutomatic(argc, argv, true);

    MPI_Finalize();
    return( 0 );
}